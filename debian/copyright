Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Xiphos
Upstream-Contact: The Gnomesword Developer Team <gnomesword-developers@lists.sourceforge.net>
Files-Excluded: win32
Comment: win32 files are excluded because they contain built festival binaries

Files: *
Copyright: Copyright 2000-2020, Xiphos Developer Team
           Copyright 2000-2020, Xiphos Development Team
License: GPL-2+

Files: debian/*
Copyright: Copyright 2009-2012, Crosswire Packaging Team <pkg-crosswire-devel@lists.alioth.debian.org>
           Copyright 2008, The GnomeSword Developer Team
           Copyright 2000-2007, Daniel Glassey <wdg@debian.org>
           Copyright 2020-2021, Bastian Germann
License: GPL-2+

Files: desktop/xiphos.appdata.xml
Copyright: 2014-2020 Xiphos Development Team <xiphos-devel@crosswire.org>
License: CC0-1.0
 On Debian systems the full text of the Creative Commons Zero 1.0 Universal
 can be found in the `/usr/share/common-licenses/CC0-1.0' file.

Files: docker-build
Copyright: Copyright 2019, Wu Xiaotian <yetist@gmail.com>
License: GPL-2+

Files: help/C/figures/index_all_pngs.py
Copyright: Copyright 2009, Simon Meers
License: Expat

Files: src/examples/ipc_client.*
Copyright: Copyright 2009, Matthew Talbert
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2+
 On Debian systems the full text of the GNU General Public License can be found
 in the `/usr/share/common-licenses/GPL-2' file.
